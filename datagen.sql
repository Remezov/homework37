CREATE TABLE IF NOT EXISTS price_list_items (
    article VARCHAR(10) NOT NULL,
    title VARCHAR(200) NOT NULL,
    price NUMERIC(7, 2)
);
ALTER TABLE price_list_items OWNER TO postgres;

CREATE TABLE IF NOT EXISTS customers (
    email VARCHAR(100) NOT NULL,
    fio VARCHAR(100) NOT NULL,
    balance NUMERIC(8, 2) NOT NULL DEFAULT 0
);
ALTER TABLE customers owner TO postgres;

CREATE TABLE IF NOT EXISTS basket_items (
    customer_email VARCHAR(100) NOT NULL, -- REFERENCES customers(email),
    article VARCHAR(10) NOT NULL -- REFERENCES price_list_items(article)
);
ALTER TABLE basket_items OWNER TO postgres;

CREATE OR REPLACE FUNCTION gen_baskets() RETURNS VOID AS $$
DECLARE
    count NUMERIC;
    rec RECORD;
BEGIN
    -- Удаление дубликатов email

    count := 0;
    LOOP
        DELETE FROM customers WHERE email IN (SELECT email FROM customers GROUP BY email HAVING count(*) > 1);
        GET DIAGNOSTICS count = ROW_COUNT;
        EXIT WHEN count = 0;
    END LOOP;

    -- Генерация корзин

    FOR rec IN SELECT email FROM customers
        LOOP
            INSERT INTO basket_items (article, customer_email)
            SELECT article, rec.email FROM price_list_items WHERE random() < 0.003;
        END LOOP;
END;
$$ LANGUAGE plpgsql;
ALTER FUNCTION gen_baskets() OWNER TO postgres;