package ru.remezov;

import java.io.FileReader;
import java.io.IOException;
import java.sql.*;
import java.util.Properties;

public class App {
    public static void main(String[] args) throws ClassNotFoundException, SQLException, IOException {
//        Class.forName("org.postgresql.Driver");

        Properties p = new Properties();
        p.load(new FileReader("jdbc.properties"));
        Connection connection = DriverManager.getConnection(
                p.getProperty("url"),
                p.getProperty("user"),
                p.getProperty("pass")
        );

        Statement statement = connection.createStatement();

//        statement.executeUpdate
//                ("insert into passenger (id_psg, name) values (38, 'Jon Smit')");

        ResultSet rs = statement.executeQuery("select * from passenger");
        while (rs.next()) {
            System.out.println(rs.getInt(1) + " | " +
                    rs.getString(2));
        }

        rs.close();
        statement.close();
        connection.close();
    }
}
