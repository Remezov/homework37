package ru.remezov;

import java.io.*;
import java.sql.*;
import java.util.Random;
import java.util.stream.Collectors;

import static com.sun.xml.internal.ws.util.StringUtils.capitalize;

public class DataGen {
    private final static String URL = "jdbc:postgresql://localhost:5432/postgres";
    private final static String USERNAME = "postgres";
    private final static String PASSWORD = "4e6Ura8kA";

    private final static String [] RU = {"аеёиоуэюя", "бвгджзйклмнпрстфхцчшщ"};
    private final static String [] ID = {"abcdefghijklmnopqrstuvwxyz0123456789_"};
    private final static String [] DOMAINS = {"@gmail.com", "@apple.com", "@mail.ru", "@yandex.ru", "@rambler.ru", "@x5.ru", "@innopolis.university"};

    public static void main(String[] args) throws SQLException, FileNotFoundException {
        try (Connection connection = DriverManager.getConnection(URL, USERNAME, PASSWORD)) {
            // Создание таблиц
            try (Statement statement = connection.createStatement()) {
                statement.executeUpdate("CREATE SCHEMA IF NOT EXISTS basket");
                connection.setSchema("basket");
                String ddl = new BufferedReader(new FileReader("datagen.sql")).lines().collect(Collectors.joining("\n"));
                statement.executeUpdate(ddl);
            }

            genPriceList(connection);
            genCustomers(connection);
            genBaskets(connection);
        }
    }

    public static void genPriceList(Connection connection) throws SQLException {
        System.out.println("Generating price list...");
        try (PreparedStatement statement = connection.prepareStatement("INSERT INTO price_list_items (article, title, price) VALUES (?, ?, ?)")) {
            connection.setAutoCommit(false);
            Random random = new Random();
            for (int j = 0; j < 1_000; j ++) {
                statement.setString(1, String.format("%010d", j));
                statement.setString(2, capitalize(word(RU)) + " " + word(RU) + " " + word(RU));
                statement.setInt(3, random.nextInt(100_000));
                statement.addBatch();
            }
            statement.executeBatch();
            connection.commit();
        }
    }

    public static void genCustomers(Connection connection) throws SQLException {
        System.out.println("Generating customers...");
        try (PreparedStatement statement = connection.prepareStatement("INSERT INTO customers (email, fio, balance) VALUES (?, ?, ?)")) {
            connection.setAutoCommit(false);
            Random random = new Random();
            for (int i = 0; i < 1_000; i ++) {
                for (int j = 0; j < 1_000; j ++) {
                    statement.setString(1, word(ID) + DOMAINS[random.nextInt(DOMAINS.length)]);
                    statement.setString(2, capitalize(word(RU)) + " " + capitalize(word(RU)) + " " + capitalize(word(RU)));
                    statement.setLong(3, random.nextInt(1_000_000));
                    statement.addBatch();
                }
                statement.executeBatch();
                connection.commit();
            }
        }
    }

    public static void genBaskets(Connection connection) throws SQLException {
        System.out.println("Generating baskets...");
        try (CallableStatement statement = connection.prepareCall("{CALL gen_baskets()}")) {
            connection.setAutoCommit(true);
            statement.executeUpdate();
        }
    }

    private static String word(String [] alphabet) {
        StringBuilder name = new StringBuilder();
        Random random = new Random();
        int length = 5 + random.nextInt(6);
        int rand01 = random.nextInt(alphabet.length);
        for (int i = 0; i < length; i ++) {
            int symbolType = (rand01 + i) % alphabet.length;
            char symbol = alphabet[symbolType].charAt(random.nextInt(alphabet[symbolType].length()));
            name.append(symbol);
        }
        return name.toString();
    }
}
